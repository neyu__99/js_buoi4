// ex1
document.getElementById('SapXep').onclick=function(){
    // input khai báo 3 số
    var Sothu1=Number(document.getElementById('Sothu1').value)
    var Sothu2=Number(document.getElementById('Sothu2').value)
    var Sothu3=Number(document.getElementById('Sothu3').value)
    // output khai báo thứ tự sắp xếp
    var Thutu
    // process chia các trường hợp dùng if else 
    if((Sothu1>Sothu2) && (Sothu1>Sothu3)){
		if(Sothu2 > Sothu3){
            document.getElementById('ThuTu').innerHTML= `${Sothu3}, ${Sothu2}, ${Sothu1}`
        }
		else{document.getElementById('ThuTu').innerHTML= `${Sothu2}, ${Sothu3}, ${Sothu1}`}
	}
	else if((Sothu2>Sothu1) && (Sothu2>Sothu3)){
		if(Sothu1 > Sothu3){
            document.getElementById('ThuTu').innerHTML= `${Sothu3}, ${Sothu1}, ${Sothu2}`
        }
		else{document.getElementById('ThuTu').innerHTML= `${Sothu1}, ${Sothu3}, ${Sothu2}`}
	}
	else if((Sothu3>Sothu1) && (Sothu3>Sothu2)){
		if(Sothu1 > Sothu2){
            document.getElementById('ThuTu').innerHTML= `${Sothu2}, ${Sothu1}, ${Sothu3}`
        }
        else{document.getElementById('ThuTu').innerHTML= `${Sothu1}, ${Sothu2}, ${Sothu3}`}
    }
    else {document.getElementById('ThuTu').innerHTML= `Có số bị trùng`}

}

// ex2
document.getElementById('Guiloichao').onclick=function(){
    // input khai báo thẻ thành viên được chọn
    var ThanhVien = document.getElementById('ThanhVien');
    // output khai báo thành viên được chọn
    var ThanhVienChon = ThanhVien.options[ThanhVien.selectedIndex].value;
    console.log("🚀 ~ file: index.js:35 ~ document.getElementById ~ ThanhVienChon", ThanhVienChon)
    // process dùng if else để đưa kết quả tương ứng người được chọn
    if (ThanhVienChon=="Me"){
        document.getElementById('ChaoHoi').innerHTML= `Xin chào mẹ`
    }
    else if (ThanhVienChon=='Bo'){
        document.getElementById('ChaoHoi').innerHTML= `Xin chào bố`
    }
    else if (ThanhVienChon=='Anhtrai'){
        document.getElementById('ChaoHoi').innerHTML= `Xin chào anh trai`
    }
    else if (ThanhVienChon=='Emgai'){
        document.getElementById('ChaoHoi').innerHTML= `Xin chào em gái`
    }
    else {
        document.getElementById('ChaoHoi').innerHTML= `Mời chọn thành viên`
    }   
}

// ex3
document.getElementById('Dem').onclick=function(){
    // input khai báo 3 số nguyên
    var ex3So1=Number(document.getElementById('ex3So1').value)
    var ex3So2=Number(document.getElementById('ex3So2').value)
    var ex3So3=Number(document.getElementById('ex3So3').value)
    // output khai bảo tổng số chẵn, tổng số lẻ
    var TongSochan=0
    var TongSole=0

    // process tính tổng số chẵn số lẻ
    if ((ex3So1%2)==0){TongSochan+=1}
    if ((ex3So2%2)==0){TongSochan+=1}
    if ((ex3So3%2)==0){TongSochan+=1}

    TongSole=3-TongSochan

    document.getElementById('SoDem').innerHTML= `Có ${TongSochan} số chẵn, ${TongSole} số lẻ`
}

// ex4
document.getElementById('DuDoan').onclick=function(){
    // input khai báo 3 cạnh tam giác
    var Canh1=Number(document.getElementById('Canh1').value)
    var Canh2=Number(document.getElementById('Canh2').value)
    var Canh3=Number(document.getElementById('Canh3').value)
    // output khai bảo kiểu tam giác
    var KieuTamgiac=''
    // process kiểm tra 3 cạnh tam giác thỏa mãn điều kiện loại nào
    if ((Canh1+Canh2>Canh3)&&(Canh1+Canh3>Canh2)&&(Canh2+Canh3>Canh1)){
        if ((Canh1==Canh2)||(Canh1==Canh3)||(Canh2==Canh3)){
            if ((Canh1==Canh2)&&(Canh1==Canh3)){document.getElementById('KieuTamgiac').innerHTML= `Tam giác đều`}
            else{
            document.getElementById('KieuTamgiac').innerHTML= `Tam giác cân`}}
        else if ((Canh1*Canh1==(Canh2*Canh2)+(Canh3*Canh3))||(Canh2*Canh2==(Canh1*Canh1)+(Canh3*Canh3))||(Canh3*Canh3==(Canh2*Canh2)+(Canh1*Canh1))){document.getElementById('KieuTamgiac').innerHTML= `Tam giác vuông`}
        else {document.getElementById('KieuTamgiac').innerHTML= `Tam giác khác`}
    }
    else {document.getElementById('KieuTamgiac').innerHTML= `Không phải tam giác`}
}
